
### Demo for poseHook v2

![recording](doc/posehook.gif)

This is a demo for the Android app [poseHook v2](https://bitbucket.org/constanze/posehook/src/master/). It consists of 3 parts.
1) the app, detecting a person and sending OSC for each body joint
2) A primitive web server relaying the OSC to websocket
3) A javascript using [p5js](https://p5js.org/) to draw the skeletons.

#### Quick start

Currently (March 2021) the demo is online. If you set the IP adress in the app to 18.192.229.240, you will
see the result at http://ec2-18-192-229-240.eu-central-1.compute.amazonaws.com:8081/

#### Installation

1. Install poseHook v2. It is currently in a early beta state, so there might be issues (bug reports
are welcome). To install the app via [Google Play](https://play.google.com/store/apps/details?id=com.hollyhook.posehook), 
you have to become a beta tester.
2. install node.js
3. clone this repository, and change to this directory
4. Run `npm install` in the terminal to install all required Node dependencies
5. In the web directory, again run `npm install` to install all web dependencies

#### Running the Demo

1. Run `node .` in the terminal
2. Open http://localhost:8081 in your browser
3. Start the app and open the _IP and port_ dialog, enter the IP of the machine where the server runs.

The app works best if the camera sees the entire body. The demo can capture a skeleton, and replay its 
movement. The capturing starts when you put your hands together above your head, for at least 1 second.
This is the code to detect the gesture:

```javascript
var eps = 30;
if (abs(this.joints[BodyPart.right_wrist][X] - this.joints[BodyPart.left_wrist][X]) < eps &&
    abs(this.joints[BodyPart.right_wrist][Y] - this.joints[BodyPart.left_wrist][Y]) < eps &&
    (this.joints[BodyPart.right_wrist][Y] < this.joints[BodyPart.right_eye][Y])) {
....
}
```
The same gesture ends the recording. The recorded data will be replayed in a loop. 




#### Inspiration

* https://www.youtube.com/watch?v=97IRtCXP8_o
* https://en.wikipedia.org/wiki/Kate_Sicchio
