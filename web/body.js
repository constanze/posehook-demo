

// used to get data out of the joints array
// numbers same as used by media pipe

const BodyPart = Object.freeze({
    "nose": 0,
    "left_eye_inner": 1, "left_eye": 2, "left_eye_outer": 3,
    "right_eye_inner": 4, "right_eye": 5, "right_eye_outer": 6,
    "left_ear": 7, "right_ear": 8,
    "mouth_left": 9, "mouth_right": 10,
    "left_shoulder": 11, "right_shoulder": 12,
    "left_elbow": 13, "right_elbow": 14,
    "left_wrist": 15, "right_wrist": 16,
    "left_pinky": 17, "right_pinky": 18,
    "left_index": 19, "right_index": 20,
    "left_thumb": 21, "right_thumb": 22,
    "left_hip": 23,   "right_hip": 24,
    "left_knee": 25,  "right_knee": 26,
    "left_ankle": 27, "right_ankle": 28,
    "left_heel": 29,  "right_heel": 30,
    "left_foot_index": 31, "right_foot_index": 32,
    "num": 33
});


// for accessing joints array
const X = 0;
const Y = 1;
const Z = 2;
const V = 3; // visibility

/** If true, gesture will trigger creation of a clone */
const CAN_SPAWN = false 

/** Time to hold the gesture until recognized */
const DEBOUNCING_TIME = 500 // ms

/** Time to delay draw on purpose, to have smoother data due to interpolation. 
 *  This is approx the duration of a framer on the mobile */
const INTERPOLATION_LAG = 80 // ms

/** states while performing a recording */
const RecordingState = Object.freeze({
            "IDLE":      0,
            "STARTING":  1, // debouncing phase of the start gesture
            "RECORDING": 2,
            "ENDING":    3, // debouncing phase of the end gesture
            "DONE":      4  // after spawn
      });


/** Number of input data per key points. Currently X,Y,Z,V. Keep consistent with the app output */
const NUM_OF_VALUES = 4;

/** Number of data sets in history for interpolation */
const NUM_OF_DATA_POINTS = 3

/** output scale to canvas pixel */
const SCALE = 800




//--- body class ------------------------------------------------------------

/**
 * represents a data set of a single person.
 */
class Body  {
    // body constructor. represents a user
    constructor(id, color, timestamp, remoteTime) {
        this.color = color
        this.birthColor = color
        this.timeOfLastData = remoteTime; // refreshed on last fill msg
        this.birthTime = remoteTime
        this.offsetAtBirth = timestamp - remoteTime
        this.id = id; 				// user id as indicated by the mobile
        this.gestureStarted = timestamp;
        this.state = RecordingState.IDLE;

        this.dataHistory = [];
        this.timeHistory = [];
        this.historyIndex = 0

        for (var i = 0; i < NUM_OF_DATA_POINTS; i++) {
            this.dataHistory.push([]) // keep the last few points for smoothing
            this.timeHistory.push(0)
            for (var j = 0; j < BodyPart.num; j++) {
                this.dataHistory[i][j] = [0, 0, 0];   // each is a list of X/Y/Z/visibility
    }
        }
        this.resetJoints() 
    }


    // reset all joints
    // be careful not to reassign joints address to history address
    resetJoints() {
        // reset joints
        this.joints = []
        for (var i = 0; i < BodyPart.num; i++) {
            this.joints.push([0, 0, 0])
            }
        }




    /**
     * called on receiving data for a single person
     * @param 33 x tuple coordinates x,y,z,v and timestamp of remote clock
     */
    fill(args, now) {
        //this.log_v("data " + args);

        var t = args[args.length - 1] // remote time base

        if (this.timeOfLastData > t) {
            // udp can be out of order
            console.log("data out of sequence " + t)
            return // drop this data
        }
        this.timeOfLastData = t; 
        this.timeHistory[this.historyIndex] = t // remote time of recording 

        for (var i = 0; i < args.length - 1; i += NUM_OF_VALUES) {
            this.dataHistory[this.historyIndex][i / NUM_OF_VALUES][X] = SCALE * args[i];
            this.dataHistory[this.historyIndex][i / NUM_OF_VALUES][Y] = SCALE * args[i+1];
            this.dataHistory[this.historyIndex][i / NUM_OF_VALUES][Z] = SCALE * args[i+2]; // not used

            /* simple way to use smoothing of the data       
            // y (n) = y (n-1) + ((x (n) - y (n-1))/smooth)
            obsolete since smothing on drawing is in place

            var smooth = 3
            jnt[X] += (scale * args[i]  - jnt[X]) / smooth;  // x
            jnt[Y] += (scale * args[i+1] - jnt[Y]) / smooth; // y
            jnt[Z] += (scale * args[i+2] - jnt[Z]) / smooth; // z */
        }
        this.historyIndex++;
        if (this.historyIndex == NUM_OF_DATA_POINTS)
            this.historyIndex = 0; // ring buffer swap

    }

    // check if current data is gesture
    // returns recorded data in case there was a spawn, otherwise returns 0
    process(args, now) {
        var ret = 0
        if (!CAN_SPAWN) return ret

        // detect trigger gesture, hands close together and above eye
        var eps = 50; // <-- AVOID absolute values
        if (abs(this.joints[BodyPart.right_wrist][X] - this.joints[BodyPart.left_wrist][X]) < eps &&
            abs(this.joints[BodyPart.right_wrist][Y] - this.joints[BodyPart.left_wrist][Y]) < eps &&
            (this.joints[BodyPart.right_wrist][Y] < this.joints[BodyPart.right_eye][Y])) {

            switch (this.state) {
            case RecordingState.IDLE:
                // start recording
                this.log_v("triggered gesture, start debouncing at " + now)
                this.gestureStarted = now
                this.state = RecordingState.STARTING // go into debouncing phase
                break;
            case RecordingState.STARTING:
                if (now > this.gestureStarted + DEBOUNCING_TIME) {
                    this.log_v("debouncing of recording start finished at " + now + ". Recording starts now.");
                    this.recordedData = [];
                    this.timestamps = [];
                    this.state = RecordingState.RECORDING;
                }
                break;
            case RecordingState.RECORDING: // back again in the gesture
                this.log_v("recording duration " + (now - this.timestamps[0]));
                if (now > this.timestamps[0] + 4000)  {
                    // minimum duration of the recording exceeded
                    this.gestureStarted = now;
                    this.log_v("debouncing of recording end at " + now);
                    this.state = RecordingState.ENDING;
                }
                break;
            case RecordingState.ENDING:
                if (now > this.gestureStarted + DEBOUNCING_TIME) {
                    this.log_v("debouncing of recording end finished at " + now + ". Spawning.");
                    // save one last set and then stop recording
                    this.recordedData.push(args)
                    this.timestamps.push(now) // last time stamp

                    // data for the clone
                    ret = {}
                    ret.id = this.id + "-spawn-" + Math.floor(100*Math.random())
                    ret.recorded = [...this.recordedData] // copy
                    ret.timestamps = [...this.timestamps]
                    this.state = RecordingState.DONE;
                }
                break;
            }
        } else {
            // not in gesture
            if (this.state == RecordingState.STARTING || this.state == RecordingState.DONE) {
                this.state = RecordingState.IDLE;
                this.log_v("back to idle")
            }

            if (this.state == RecordingState.ENDING) {
                this.state = RecordingState.RECORDING;
                this.log_v("back to recording");
            }
        }

        // save points while recording
        if (this.state == RecordingState.RECORDING ||
                this.state == RecordingState.ENDING) {
            this.recordedData.push(args)
            this.timestamps.push(now)
        }
        return ret
    }


    setMyColor(now) {
        var age = now - this.timeOfLastData;

        if (age < 60000) { // less than 1 minute old
            // colors for debugging
            switch (this.state) {
                default:
                    this.color = this.birthColor;
                    break;
                case RecordingState.RECORDING:
                    this.color = color(200);
                    break;
                case RecordingState.STARTING:
                case RecordingState.ENDING:
                    this.color = color(100);
                    break;
            }
        } else {
            // elderly
            if (this.state != RecordingState.IDLE && this.state != RecordingState.DONE) {
                // we lost connection while in recording.
                this.log_v("this recording is of age " + age + ", and doesn't get any more data. dropping")
                this.state = RecordingState.DONE;
                this.recordedData = []
                this.timestamps = []
            }
            // fade away if no data received
            var alpha = min(1.0, max(0.6, (5 - age/1000.0)/5.0))
            var c = 'rgba(' + this.birthColor._getRed() + ', ' +
                              this.birthColor._getGreen() + ', ' +
                              this.birthColor._getBlue() + ', ' +
                              alpha +
                    ')';
            this.color = color(c);
        }
    }



    
    // draw match stick men - you need to overwrite this
    draw() {
        this.log_v("abstract function called")
    }


    /** 
     * Linear interpolation, of the 3 measurements we saved.
     * Accepting a lag of ca 100 ms. The result is saved in this.joints
    */
    interpolate(now) {

        var t = now - this.offsetAtBirth // switching to remote time stamp, which are more precise
        var tDraw = t - INTERPOLATION_LAG // we draw a past body, from 80ms ago. 

        var i1 = 0
        var i2 = 0
        if (this.timeHistory.length < NUM_OF_DATA_POINTS) return

        // historyIndex points to the next to be overwritten
        // so the one before is the newest
        var newestIndex = (this.historyIndex + NUM_OF_DATA_POINTS - 1) % NUM_OF_DATA_POINTS

        if (tDraw >= this.timeHistory[newestIndex]) {
            // nothing to interpolate, we didn't receive any recent data 
            i1 = midIndex
            i2 = newestIndex
            //console.log("no recent data tDraw " + tDraw + " last Data " + this.timeHistory[newestIndex])
        } else {
            // same as this.historyIndex
            var oldestIndex = (newestIndex + NUM_OF_DATA_POINTS - 2) % NUM_OF_DATA_POINTS
            var midIndex = (newestIndex + NUM_OF_DATA_POINTS - 1) % NUM_OF_DATA_POINTS

            // where are we ?
            if (tDraw < this.timeHistory[midIndex] && tDraw >= this.timeHistory[oldestIndex]) {
                // in between, slow 
                i1 = oldestIndex
                i2 = midIndex
            } else if (tDraw >= this.timeHistory[midIndex]) {
                // nice and fast
                i1 = midIndex
                i2 = newestIndex
            } else {    
                // our data points are all more recent than tDraw, should not happen
                i1 = oldestIndex
                i2 = midIndex
            }

            // time interval length = 100% 
            // TODO use also the 3rd point to interpolate?
            var t100 = this.timeHistory[i2] - this.timeHistory[i1]
            if (t100 == 0) {
                // would be div by zero, just take one and copy
                for (var i = 0; i < BodyPart.num; i++)
                    for (var j = 0; j < NUM_OF_VALUES; j++)
                        this.joints[i][j] = this.dataHistory[i2][i][j]

            } else {
                // example  oldest 1447809 mid 1447908 tDraw 1447939 newest 1448012
                // i1 = mid, i2 = newest
                // tx = 1447939 - 1447908 = 31
                // t100 = 1448012 - 1447908 = 103
                // x =  31 / 103 = 0.3

                var tx = tDraw - this.timeHistory[i1]
                var x = tx / t100

                // now calculate interpolation of each joint
                for (var i = 0; i < BodyPart.num; i++) {
                    for (var j = 0; j < NUM_OF_VALUES; j++) {
                        this.joints[i][j] =
                            this.dataHistory[i1][i][j] + x * (this.dataHistory[i2][i][j] - this.dataHistory[i1][i][j]);
    }
                }
            }

            /* debug code, too noisy
            var i = 0
            console.log("\t" + i1 + "\t" + i2 + "\t" +
                tDraw + "\t" + this.timeHistory[i1] + "\t" + this.timeHistory[i2] + "\t" +
                this.joints[i][0] + "\t" + this.dataHistory[i1][i][0] + "\t" + this.dataHistory[i2][i][0] + "\t" +
                this.joints[i][1] + "\t" + this.dataHistory[i1][i][1] + "\t" + this.dataHistory[i2][i][1] + "\t")
                */

        }
    }
    

    // verbose debug output 
    log_v(txt) {
        console.log(this.id + ": " + txt)
    }

}

/**
 * this is a clone, instead of receiving new data it replays old
 */
class Clone extends Body {
    constructor(id, color, timestamp, recorded, timestamps) {
        super(id, color, timestamp);
        this.log_v("Hello, I'm a clone " + this.id +
              ". i have " + timestamps.length + " data points");

        this.recordedJoints = recorded;
        this.recordedTimestamps = []

        // normalize timestamps
        for (var i = 0; i < timestamps.length; i++) {
            this.recordedTimestamps[i] = timestamps[i] - timestamps[0];
        }
    }

    process(now) {} // overwrite as empty

    /** draw the recorded data, according to the time stamp
    /* TODO interpolation missing */
    draw(now) {
        var offset = now % this.recordedTimestamps[this.recordedTimestamps.length-1];

        // find where we are right now
        for (var i = 0; i < this.recordedTimestamps.length-1; i++) {
            if ((offset >= this.recordedTimestamps[i]) && (offset < this.recordedTimestamps[i+1])) {
                this.fill(this.recordedJoints[i], now);
                super.draw(now)
                break;
            }
        }
    }
}




function log_e(txt) {
    console.error(txt)
}