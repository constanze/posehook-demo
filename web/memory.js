
planck.testbed('Boxes', function (testbed) {
    // simpler variant
    // const colors = ["#f94144", "#f3722c", "#f8961e", "#f9844a", "#f9c74f", "#90be6d", "#43aa8b", "#4d908e", "#577590", "#277da1"]
    const colors = ["#FE2712", "#E3F7D4", "#4424D6", "#FCCB1A",
        "#AE0D7A", "#CBE432", "#000000", "#ffffff",
        "#448D76", "#FD4D0C", "#700CBC", "#F7F4D4",
        "#B88114"];


    const NUM = colors.length * 2
    const NUM_X = NUM / 4
    const NUM_Y = NUM / NUM_X
    const BOXLEN = 3
    const FRAMELENX = 60
    const FRAMELENY = 30
    let cards = []
    let prevIndex = -1;

    // ---- class for each of the cards -------------
    class Card {
        constructor(x, y, len, color) {
            this.x = x;
            this.y = y;
            this.len = len
            this.color = color
            this.open = false
            this.paired = false
            this.body = world.createBody().setDynamic();
            this.body.createFixture(pl.Box(BOXLEN, BOXLEN));
            this.body.setPosition(Vec2(BOXLEN * 1.1 + (x - NUM_X / 2) * (BOXLEN * 2.2),
                BOXLEN * 1.1 + (y - NUM_Y / 2) * (BOXLEN * 2.2)))
            this.body.setMassData({
                mass: 1,
                center: Vec2(),
                I: 1
            })
            this.body.render = { fill: this.open ? this.color : '#555555',  stroke: '#999999' }
        }

        // is mouseclick inside my card?
        isHit(point) {
            var f = this.body.getFixtureList(); // only one
            var center = this.body.getPosition();
            if ((point.x >= center.x - BOXLEN) && (point.x <= center.x + BOXLEN) &&
                (point.y >= center.y - BOXLEN) && (point.y <= center.y + BOXLEN))
                return true;           
            return false;
        }

        // change color
        turn() {
            this.open = !this.open
            this.body.destroyFixture(this.body.getFixtureList())         
            this.body.render.fill = this.open ? this.color : '#555555';
            this.body.createFixture(pl.Box(BOXLEN, BOXLEN));
        }
    }


    var pl = planck, Vec2 = pl.Vec2;
    var world = pl.World({});
    testbed.y = 0;
    testbed.width = 60;
    startGame()
    // if clicked on a box, color it, and find matches
    testbed.canvas.addEventListener('mouseup', function (e) {
        var point = world2planck({ x: e.clientX, y: e.clientY });

        var hitIndex = -1;
        var numOfOpen = 0;

        for (var i = 0; i < NUM; i++) {
            if (cards[i].isHit(point)) {
                if (cards[i].open == false) {
                    cards[i].turn();
                    hitIndex = i;
                }
            }
        }

        if (hitIndex == -1)
            return; // no card was hit

        // look for another card with the same color
        for (var j = 0; j < NUM; j++) {
            if (j !== hitIndex &&
                cards[hitIndex].color === cards[j].color &&
                cards[hitIndex].open === true &&
                cards[j].open === true) {
                // heureka
                console.log("found a pair with " + cards[j].color)
                cards[j].paired = true
                cards[hitIndex].paired = true
            }

            if (cards[j].open === true) {
                numOfOpen++; // numOfOpen = numOfOpen + 1
            }
        }

        console.log("count of open cards " + numOfOpen)
        console.log("last time " + prevIndex + " this time " + hitIndex)

        for (var i = 0; i < NUM; i++) {
            if (cards[i].open == true &&
                cards[i].paired == false &&
                i != hitIndex && (numOfOpen % 2 == 0 || i != prevIndex))
                cards[i].turn()
        }
        prevIndex = hitIndex;

        if (numOfOpen == NUM)
            setTimeout(function () {
                startGame();
            }, 3000)

    });
        
    return world

    // point from canvas coordinates to planck coordinates
    function world2planck(point) {
        // the smaller size is taken for scaling
        var center = { x: testbed.canvas.clientWidth / 2, y: testbed.canvas.clientHeight / 2 }
        var scale = Math.min (testbed.canvas.clientWidth, testbed.canvas.clientHeight)
        var planckX = testbed.width * ((point.x - center.x) / scale);
        var planckY = testbed.height * ((center.y - point.y) / scale);

        return {
            x: planckX,
            y: planckY
        }
    }

    function startGame() {
        // clear cards
        for (var i = 0; i < NUM; i++) cards[i] = 0;

        // destroy all bodies
        for (let b = world.getBodyList(); b; b = b.getNext()) {
            world.destroyBody(b); 
        }

        var bar = world.createBody();
        bar.createFixture(pl.Edge(Vec2(-FRAMELENX, -FRAMELENY), Vec2(+FRAMELENX, -FRAMELENY)));
        bar.createFixture(pl.Edge(Vec2(+FRAMELENX, -FRAMELENY), Vec2(+FRAMELENX, +FRAMELENY)));
        bar.createFixture(pl.Edge(Vec2(+FRAMELENX, +FRAMELENY), Vec2(-FRAMELENX, +FRAMELENY)));
        bar.createFixture(pl.Edge(Vec2(-FRAMELENX, +FRAMELENY), Vec2(-FRAMELENX, -FRAMELENY)));

        for (var i = 0; i < NUM; i++) {
            // find a free spot in the array
            var found = false
            while (!found) {
                candidate = Math.floor(Math.random() * NUM);
                if (cards[candidate] == 0) {
                    found = true;
                    var x = candidate % NUM_X;
                    var y = Math.floor(candidate / NUM_X);
                    // create a new card
                    card = new Card(x, y, BOXLEN, colors[i % colors.length])
                    cards[candidate] = card
                }
            }
        }
    }

});
