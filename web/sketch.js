

var persons = {}
let colors = []
let dataTimestampLocal = 0
let dataTimestampRemote = 0

function setup() {
    createCanvas(1000, 800);

    // some random color palette
    colors = [
        color('#690e24'),
        color('#721b1d'),
        color('#922f22'),
        color('#a44220'), // orange
        color('#ac7c26'), // yellow
        color('#143130'), // green
        color('#153326'),
        color('#0c3041')  // türkis
    ]

    colorMode(RGB, 255, 255, 255, 1);

    let host = location.origin.replace(/^http/, 'ws');
    console.log("connecting WebSocket to host " + host);

    this.oscPort = new osc.WebSocketPort({
        url: host
    });

    this.oscPort.open();
    this.oscPort.on("message", function (msg) {
        //console.log(msg);
        if (msg.address.indexOf("/posehook/") == 0) {
            var id = msg.address.split('/')[2]
            var now = Date.now()
            var nowRemote = msg.args[msg.args.length - 1] // last arg is the remote timestamp

            // look if a person with this id is already existing
            if (persons.hasOwnProperty(id)) {
                // save the data.
                persons[id].fill(msg.args, now);
                
                // this looks for gestures
                var cloneData = persons[id].process(msg.args, now);
                if (cloneData) {
                    log_v("add a clone to my list "+ cloneData.id)
                    persons[cloneData.id] = new Clone(cloneData.id,
                        colors[Object.keys(persons).length % colors.length],
                        now, cloneData.recorded,
                        cloneData.timestamps);
                }
            } else {
                // create a new one, pick the next color in the palette
                log_v("new person " + id + " birth time " + nowRemote);
                persons[id]= new Body(id, colors[Object.keys(persons).length % colors.length], now, nowRemote);
            }

            dataTimestampLocal = now
            dataTimestampRemote = nowRemote

        }
    });
}

// p5 calls this
function draw() {
    var now = Date.now()
    background(5);
    for (var id in persons) {
        persons[id].interpolate(now) // smooth data. 
        persons[id].draw()
    }
    text(getFrameRate(), 10, 750)
}

function log_v(txt) {
    console.log(txt)
}


//---------- all needed for drawing starts here ----------------------



Body.prototype.draw_line = function(from, to) {
    line(this.joints[from][X], this.joints[from][Y],
         this.joints[to][X], this.joints[to][Y]);
    strokeWeight(20)
    stroke(this.color)
}


// overwrite 
Body.prototype.draw = function() {
    noFill();
    
    // draw match stick men in color this.color
    push()
    //this.setMyColor(now);

    this.draw_line(BodyPart.left_ear,        BodyPart.left_eye);
    this.draw_line(BodyPart.left_eye,        BodyPart.left_eye_inner);
    this.draw_line(BodyPart.left_eye_inner,  BodyPart.nose);

    this.draw_line(BodyPart.right_ear,       BodyPart.right_eye);
    this.draw_line(BodyPart.right_eye,       BodyPart.right_eye_inner);
    this.draw_line(BodyPart.right_eye_inner, BodyPart.nose);

    this.draw_line(BodyPart.left_shoulder,   BodyPart.right_shoulder);

    this.draw_line(BodyPart.right_shoulder,  BodyPart.right_elbow);
    this.draw_line(BodyPart.right_elbow,     BodyPart.right_wrist);
    this.draw_line(BodyPart.right_wrist,     BodyPart.right_index);

    this.draw_line(BodyPart.left_shoulder,   BodyPart.left_elbow);
    this.draw_line(BodyPart.left_elbow,      BodyPart.left_wrist);
    this.draw_line(BodyPart.left_wrist,      BodyPart.left_index);

    this.draw_line(BodyPart.left_hip,        BodyPart.right_hip);

    this.draw_line(BodyPart.right_hip,       BodyPart.right_knee);
    this.draw_line(BodyPart.right_knee,      BodyPart.right_ankle);
    this.draw_line(BodyPart.right_ankle,     BodyPart.right_heel);
    this.draw_line(BodyPart.right_heel,      BodyPart.right_index); // bug

    this.draw_line(BodyPart.left_hip,        BodyPart.left_knee);
    this.draw_line(BodyPart.left_knee,       BodyPart.left_ankle);
    this.draw_line(BodyPart.left_ankle,      BodyPart.left_heel);
    this.draw_line(BodyPart.left_heel,       BodyPart.left_index);
    pop();
}


// draw curved lines between the body parts, expects a list of body part ids
Body.prototype.drawCurve = function(bodyPoints) {
    beginShape()
    curveVertex(this.joints[bodyPoints[0]][X], this.joints[bodyPoints[0]][Y])
    for (var bp = 0; bp < bodyPoints.length; bp++) {
        curveVertex(this.joints[bodyPoints[bp]][X], this.joints[bodyPoints[bp]][Y])
    }
    curveVertex(this.joints[bodyPoints[bodyPoints.length - 1]][X], this.joints[bodyPoints[bodyPoints.length - 1]][Y])
    endShape()
}

// draw a straight connection between 2 body parts
Body.prototype.drawLine= function(from, to) {
    line(this.joints[from][X], this.joints[from][Y],
            this.joints[to][X], this.joints[to][Y]);
    
}
